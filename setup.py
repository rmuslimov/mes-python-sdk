from setuptools import setup

setup(
    name='mes-python-sdk',
    version='0.1',
    description='Trivial access to MES.',
    author='mes',
    author_email='mesmes@gmail.com',
    url='https://bitbucket.org/merchantesolutions/mes-python-sdk.git',
    packages=[
        'mes_python_sdk',
    ],
    install_requires=[
        'httplib2==0.9.2',
    ],
)
