'''
See a MeS certification manager, or email meS_certification@merchante-solutions.com for a testing profileid & key
Created on Dec 24, 2012
@author: brice
'''

from gateway.Gateway import GatewayRequest, TransactionType, GatewayUrl

g = GatewayRequest(GatewayUrl.Cert, TransactionType.Tokenize)
g.addCredentials('profileid', 'profilekey')
g.addCardData('4012888812348882', '1216')
saleResp = g.run()

print('Tokenize error code: '+saleResp.getErrorCode())
print('Tokenize resp text: '+saleResp.getRespText())

g = GatewayRequest(GatewayUrl.Cert, TransactionType.Sale)
g.addCredentials('profileid', 'profilekey')
g.addTokenData(saleResp.getTranId(), '1216')
g.addAmount('1.00')
refundResp = g.run()

print('Sale error code: '+refundResp.getErrorCode())
print('Sale resp text: '+refundResp.getRespText())