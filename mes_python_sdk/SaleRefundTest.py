'''
See a MeS certification manager, or email meS_certification@merchante-solutions.com for a testing profileid & key
Created on Dec 24, 2012
@author: brice
'''

from gateway.Gateway import GatewayRequest, TransactionType, GatewayUrl

g = GatewayRequest(GatewayUrl.Cert, TransactionType.Sale)
g.addCredentials('profileid', 'profilekey')
g.addCardData('4012888812348882', '1216')
g.addAvsData("123 N. Main", '55555')
g.addAmount('1.00')
saleResp = g.run()

print('Sale error code: '+saleResp.getErrorCode())
print('Sale resp text: '+saleResp.getRespText())

g = GatewayRequest(GatewayUrl.Cert, TransactionType.Sale)
g.addCredentials('profileid', 'profilekey')
g.addTranId(saleResp.getTranId())
refundResp = g.run()

print('Refund error code: '+refundResp.getErrorCode())
print('Refund resp text: '+refundResp.getRespText())