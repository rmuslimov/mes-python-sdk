'''
Created on Dec 24, 2012

@author: brice
'''
from mes_python_sdk.core.Http import LibHttp

class GatewayRequest:
    def __init__(self, gatewayUrl, transactionType):
        self.requestParameters = []
        self.addParameter('transaction_type', transactionType)
        self.hostUrl = gatewayUrl

    def addParameter(self, name, value):
        namePair = [name, value]
        self.requestParameters.append(namePair)

    def requestString(self):
        request = ''
        for i in range(len(self.requestParameters)):
            request = request + self.requestParameters[i][0] + '=' + self.requestParameters[i][1] + '&'
        return request[0:-1]

    def hostUrl(self, gatewayUrl):
        self.hostUrl = gatewayUrl

    def addCredentials(self, profileId, profileKey):
        self.addParameter('profile_id', profileId)
        self.addParameter('profile_key', profileKey)

    def addCardData(self, cardNum, expDate):
        self.addParameter('card_number', cardNum)
        self.addParameter('card_exp_date', expDate)

    def addTokenData(self, token, expDate):
        self.addParameter('card_id', token)
        self.addParameter('card_exp_date', expDate)

    def addAvsData(self, address, zipCode):
        self.addParameter('cardholder_street_address', address)
        self.addParameter('cardholder_zip', zipCode)

    def addInvoice(self, invoice):
        self.addParameter('invoice_number', invoice)

    def addClientRef(self, ref):
        self.addParameter('client_reference_number', ref)

    def addAmount(self, amount):
        self.addParameter('transaction_amount', amount)

    def addTranId(self, tranId):
        self.addParameter('transaction_id', tranId)

    def run(self):
        h = LibHttp(self.hostUrl, self.requestString())
        h.run()
        return GatewayResponse(h.rawResponse)

class GatewayResponse:
    def __init__(self, responseString):
        self.responseList = {}
        conv = unicode(responseString)
        s = conv.split('&')
        for i in range(len(s)):
            npv = s[i].split('=')
            self.responseList[npv[0]] = npv[1]

    def getValue(self, name):
        return self.responseList[name]

    def getRespText(self):
        return self.getValue('auth_response_text')

    def getTranId(self):
        return self.getValue('transaction_id')

    def getErrorCode(self):
        return self.getValue('error_code')

    def getAvsResult(self):
        return self.getValue('avs_result')

    def getCvvResult(self):
        return self.getValue('cvv2_result')

    def getAuthCode(self):
        return self.getValue('auth_code')

    def isApproved(self):
        code = self.getErrorCode()
        return code == '000' or code == '085'

class TransactionType:
    Sale = 'D'
    Preauth = 'P'
    Settle = 'S'
    Reauth = 'J'
    Offline = 'O'
    Void = 'V'
    Credit = 'C'
    Refund = 'U'
    Verify = 'A'
    Tokenize = 'T'
    Detokenize = 'X'
    Batchclose = 'Z'

class GatewayUrl:
    Cert = 'https://cert.merchante-solutions.com/mes-api/tridentApi'
    Test = 'https://test.merchante-solutions.com/mes-api/tridentApi'
    Live = 'https://api.merchante-solutions.com/mes-api/tridentApi'
